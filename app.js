const createError = require("http-errors");
const express = require("express");
const path = require("path");
const fs = require("fs");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const sassMiddleware = require("node-sass-middleware");
const helpers = require("./helpers");

const gettingStarted = require("./routes/getting_started");
const components = require("./routes/components");
const utilities = require("./routes/utilities");
const theme = require("./routes/theme");

const app = express();

global.__basedir = __dirname;

// View engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(sassMiddleware({
    src: path.join(__dirname, "public"),
    dest: path.join(__dirname, "public"),
    indentedSyntax: true, // True = SASS and False = SCSS
    sourceMap: true
}));
app.use(express.static(path.join(__dirname, "public")));

let index = express.Router();

index.get("/", function (req, res, next) {
    res.redirect("/getting-started");
});


// Create the search bar items dynamically based of the examples folder
app.use((req, res, next) => {
    // Get all the folders within the components folder
    let components = fs.readdirSync("public/examples/components", { withFileTypes: true })
        .filter(dirent => dirent.isDirectory())
        .map(dirent => dirent.name);

    // Get all the folders within the utilities folder
    let utilities = fs.readdirSync("public/examples/utilities", { withFileTypes: true })
        .filter(dirent => dirent.isDirectory())
        .map(dirent => dirent.name);

    // Add a global variables to pass to each template
    res.locals.searchItems = {
        "Getting Started": [{name: "Getting Started", link: "/getting-started"}],
        "Components": components.map((item, i) => ({
            name: helpers.convertFileNameToTitle(item, false),
            link: `/components/${item}`,
        })),
        "Utilities": utilities.map((item, i) => ({
            name: helpers.convertFileNameToTitle(item, false),
            link: `/utilities/${item}`,
        })),
        "Theme": [{name: "Theme", link: "/theme"}],
    };

    next();
});

app.use("/", index);
app.use("/getting-started", gettingStarted);
app.use("/components", components);
app.use("/utilities", utilities);
app.use("/theme", theme);

// Catch 404 and forward to error handler
app.use((req, res, next) => {
    next(createError(404));
});

// Error handler
app.use((err, req, res, next) => {
    // Set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get("env") === "development" ? err : {};

    // Render the error page
    res.status(err.status || 500);
    res.render("error");
});

module.exports = app;
