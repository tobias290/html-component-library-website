const express = require("express");
const router = express.Router();
const helpers = require("../helpers");
const fs = require("fs");


// Get all component files from the examples directory
let components = fs.readdirSync("public/examples/components", { withFileTypes: true })
    .filter(dirent => dirent.isDirectory())
    .map(dirent => dirent.name);

// Loop over each component get each example from the file and dynamically generate HTML/Pug files
for (let item of components) {
    router.get(`/${item}`, function (req, res, next) {
        let examples = [];


        // Get each html file in the components file
        fs.readdirSync(`public/examples/components/${item}`).forEach(file => {
            examples.push({
                title: helpers.convertFileNameToTitle(file).substr(2),
                pageLink: file.split(".")[0],
                html: fs.readFileSync(`public/examples/components/${item}/${file}`)
            });
        });

        // Render the file
        res.render("component-utility", {
            title: helpers.convertFileNameToTitle(item, false),
            currentNav: `component-${item}`,
            examples: examples
        });
    });
}


module.exports = router;
