const express = require("express");
const fs = require("fs");
const archiver = require("archiver");
const sass = require("node-sass");
const compressor = require("node-minify");
const helpers = require("../helpers");
const router = express.Router();


router.get("/", function (req, res, next) {
    res.render("getting-started", {
        title: "Getting Started",
        currentNav: "getting-started",
    });
});

router.get("/download", function (req, res, next) {
    let baseDir = "public/downloads/html-component-library";

    let theme = Object.keys(req.query).length === 0 ? {
        primary: "3f8aec",
        secondary: "6c757d",
        success: "38ae48",
        info: "1b93b2",
        warning: "c4b924",
        danger: "cb2a2a",
        light: "f9f9f9",
        dark: "3b3b3b",
    } : req.query;

    // Create a zip file
    let output = fs.createWriteStream(`${baseDir}.zip`);

    let archive = archiver("zip", {
        zlib: {level: 9} // Sets the compression level.
    });

    archive.on("error", function (err) {
        throw err;
    });

    // Update the variables scss file with the updated theme
    fs.writeFileSync(
        `${baseDir}/scss/partials/_variables.scss`,
        helpers.createSassVariableFile(theme)
    );

    // Render the result and convert it to CSS
    sass.render({
        file: `${global.__basedir}/${baseDir}/scss/styles.scss`
    }, (err, result) => {
        if (err)
            return console.log(err);

        // Write the CSS to a file
        fs.writeFileSync(
            `${baseDir}/dist/css/styles.css`,
            result.css.toString()
        );

        // Minify the CSS
        compressor.minify({
            compressor: "clean-css",
            input: `${global.__basedir}/${baseDir}/dist/css/styles.css`,
            output: `${global.__basedir}/${baseDir}/dist/css/styles.min.css`,
            callback: function (err, min) {
                if (err)
                    return console.log(err);

                // Take the files and add it to the ZIP file
                archive.pipe(output);
                archive.directory(`${baseDir}/`, false);
                archive.finalize();

                // Watch for the downloads folder to be updated then download the zip file
                fs.watch("public/downloads", (event, fileName) => {
                    res.download(`${global.__basedir}/public/downloads/html-component-library.zip`, (err) => {
                        if (err)
                            console.log(err);
                    });
                });
            }
        });
    });
});

module.exports = router;
