let express = require("express");
let router = express.Router();
let fs = require("fs");

// Render theme page
router.get("/", function (req, res, next) {
    res.render("theme", {
        title: "Theme",
        currentNav: "theme",
        example: fs.readFileSync("public/examples/theme/theme.html")
    });
});

module.exports = router;
