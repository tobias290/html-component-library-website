function downloadTheme(event) {
    event.preventDefault();

    let queryString = "";

    for (let input of event.target.elements) {
        if (input.type !== "color") continue;

        queryString += `${input.name}=${input.value.slice(1)}&`
    }

    queryString = queryString.slice(0, -1);


    let link = document.createElement('a');
    link.href= `http://${window.location.host}/getting-started/download/?${queryString}`;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}
