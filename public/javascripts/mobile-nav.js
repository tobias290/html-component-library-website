/**
 * Toggles the mobile navigation page.
 */
function toggleMobileNav() {
    let mobileNav = document.getElementById("mobile-nav");

    mobileNav.classList.toggle("mobile-nav--hide");

    for (let ham of document.getElementsByClassName("hamburger"))
        ham.classList.toggle("hamburger--active");
}
