function copy(code) {
    let element = document.createElement("textarea");
    document.body.appendChild(element);
    element.value = code;
    element.select();
    document.execCommand("copy");
    document.body.removeChild(element);
}

