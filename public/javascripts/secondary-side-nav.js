/**
 * Active the item currently being views.
 */
document.onscroll = () => {
    let titles = document.getElementsByClassName("section__title");
    let navItems = document.getElementsByClassName("secondary-side-nav__list-item");

    for (let index in titles) {
        if (!titles.hasOwnProperty(index))
            continue;

        let titleRect = titles[index].getBoundingClientRect();

        if (titleRect.top <= 0) {
            for (let navItem of navItems)
                navItem.classList.remove("secondary-side-nav__list-item--active");

            navItems[index].classList.add("secondary-side-nav__list-item--active");
        }
    }
};
