/**
 * Displays the sub list.
 *
 * @param el - Element clicked.
 */
function showSubItem(el) {
    el.classList.toggle("nav__item--display");
}
