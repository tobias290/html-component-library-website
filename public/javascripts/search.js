/**
 * Activated on search.
 * Updates the data list based of the search query.
 *
 * @param {HTMLInputElement} element - Input element that is the search input.
 */
function onSearch(element) {
    let query = element.value.toString().toLowerCase();
    let headers = document.getElementsByClassName("search-list__header");
    let items = document.getElementsByClassName("search-list__item");


    for (let item of items) {
        if (query === "") {
            item.style.display = "block";
            continue;
        }

        item.style.display = item.getAttribute("data-item").toLowerCase().includes(query) ? "block" : "none";
    }

    for (let header of headers) {
        let items = document.querySelectorAll(`[data-header="${header.innerText}"]`);

        header.style.display = Array.from(items).every(item => item.style.display === "none") ? "none" : "block";
    }
}
