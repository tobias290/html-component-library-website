class Hamburger {
    /**
     * @param {HTMLElement} hamburger - Hamburger element.
     */
    constructor(hamburger) {
        this.hamburger = hamburger;
    }

    /**
     * Toggles the hamburger element from open or close state to the opposite state.
     */
    toggle() {
        this.hamburger.classList.toggle("hamburger--active");
    }
}
