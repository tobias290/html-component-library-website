exports.capitalize = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
};

/**
 * Takes the name of a example file and converts its file name into a title.
 *
 * @param {string} fileName - Name of the file.
 * @param {boolean} hasExtension - True if file has type extension or not.
 * @returns {string}
 */
exports.convertFileNameToTitle = (fileName, hasExtension= true) => {
    if (hasExtension)
        // Remove file extension
        fileName = fileName.split(".")[0];

    let newTitle = "";

    for (let word of fileName.split("-")) {
        newTitle += " " + (word.charAt(0).toUpperCase() + word.slice(1));
    }

    return newTitle;
};

/**
 * Creates a new theme based of the colors given.
 * Takes the colors and create a new _variables.scss file.
 *
 * @param {Object} colors - Colors object.
 * @returns {string} - Returns string representation of the file.
 */
exports.createSassVariableFile = (colors) => {
    return `
        /// Colors
        
        $primary: #${colors["primary"]};
        $secondary: #${colors["secondary"]};
        $success: #${colors["success"]};
        $info: #${colors["info"]};
        $warning: #${colors["warning"]};
        $danger: #${colors["danger"]};
        $light: #${colors["light"]};
        $dark: #${colors["dark"]};
        $link: #007BFF;
        $white: #FFF;
        $black: #000;
        $disabled: #808080;
        $component-border: #D1D1D1;
        
        $colors: (
            primary: $primary,
            secondary: $secondary,
            success: $success,
            info: $info,
            warning: $warning,
            danger: $danger,
            dark: $dark,
            light: $light,
            white: $white,
            black: $black,
        );
        
        /// Font Sizes
        
        $root-desktop-font-size: 16px;
        $root-mobile-font-size: 12px;
        $base-font-size: 1rem;
        
        $alert-font-size: $base-font-size;
        
        $breadcrumb-font-size: 1.25rem;
        
        $button-font-size: $base-font-size;
        
        $card-header-font-size: 1.25rem;
        $card-title-font-size: 1.25rem;
        $card-content-font-size: $base-font-size;
        
        
        $list-heading-font-size: $base-font-size;
        $list-item-font-size: $base-font-size;
        
        $modal-header-font-size: 1.25rem;
        $modal-content-font-size: $base-font-size;
        
        $pagination-font-size: $base-font-size;
        
        $progress-bar-percentage-font-size: .75rem;
        
        $tooltip-font-size: .75rem;
        
        $widget-title-font-size: $base-font-size;
        $widget-content-font-size: $base-font-size;
        
        
        /// Grids
        
        $grid-gutters: (
            xxsmall: 1px,
            xsmall: 2px,
            small: 5px,
            medium: 10px,
            large: 15px,
            xlarge: 20px,
            xxlarge: 30px,
        );
        
        /// Breakpoints
        
        $breakpoint-xxs: 320px;
        $breakpoint-xs: 480px;
        $breakpoint-sm: 640px;
        $breakpoint-md: 768px;
        $breakpoint-lg: 960px;
        $breakpoint-xl: 1180px;
        $breakpoint-xxl: 1400px;
        
        $breakpoints: (
            xxs: $breakpoint-xxs,
            xs: $breakpoint-xs,
            sm: $breakpoint-sm,
            md: $breakpoint-md,
            lg: $breakpoint-lg,
            xl: $breakpoint-xl,
            xxl: $breakpoint-xxl
        );
        
        
        /// Other
        
        $default-font-family: "Raleway", sans-serif;
        
        $default-border-radius: 5px;
        
        $triangle-directions: (up: "up", down: "down", left: "left", right: "right");
    `;
};
